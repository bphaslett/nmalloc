#!/bin/bash

export PREFIX=/usr/local
export LIBDIR=/lib/x86_64-linux-gnu

LD_PRELOAD="${LIBDIR}/libpthread.so.0:${PREFIX}${LIBDIR}/libnmalloc.so" exec "$@"
