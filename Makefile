# if PREFIX or LIBDIR are changed here, change them in the shellscript too
PREFIX := /usr/local
LIBDIR := ${PREFIX}/lib/x86_64-linux-gnu
BINDIR := ${PREFIX}/bin
INCLUDEDIR := ${PREFIX}/include
CFLAGS :=  -Wno-deprecated-declarations -O0 -ggdb3 -fstack-protector-all
LIBFLAGS := -fpic -shared
CC := gcc

all: libnmalloc.so heap_overflow

heap_overflow:%:%.c
	$(CC) -o $@ $<

libnmalloc.so:nmalloc.c nmalloc.h
	$(CC) $(CFLAGS) $(LIBFLAGS) -o $@ $<

install: libnmalloc.so
	install libnmalloc.so ${LIBDIR}
	install guard.sh ${BINDIR}

clean: 
	rm libnmalloc.so
