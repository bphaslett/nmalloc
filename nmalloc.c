/*
 * Copyright (C) 2014-2021 Free Software Foundation, Inc.
 *
 * nmalloc 4.0
 *
 * Created Jun 5, 2014 by Su Yu
 * Modified by Brian Haslett
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "nmalloc.h"

unsigned long *canary;
//#define DEBUG

void nmalloc_init_hook(void)
{
    int ret=0;
    pthread_attr_init(&attr);
    pthread_mutexattr_init(&tma);
//    pthread_mutexattr_init(&ama);
    pthread_mutexattr_settype(&tma, PTHREAD_MUTEX_NORMAL);
//    pthread_mutexattr_settype(&ama, PTHREAD_MUTEX_RECURSIVE);
    pthread_mutex_init(&table_mutex, &tma);
//    pthread_mutex_init(&alloc_mutex, &ama);

    canary =  malloc(sizeof(unsigned long));
    ret = getrandom(canary, sizeof(unsigned long), 0);
    if (ret <= 0) {
        fprintf(stderr, "Error, failed in nmalloc_init_hook\n");
        return;
    }

    sys_malloc_hook = __malloc_hook;
    sys_free_hook = __free_hook;
    sys_realloc_hook = __realloc_hook;

    __malloc_hook = nmalloc_hook;
    __free_hook = nfree_hook;
    __realloc_hook = nrealloc_hook;
}

void *nmalloc_hook(size_t size, const void *caller)
{
//    pthread_t thread;
    struct arg_struct args;

    if (!__malloc_hook) {
        args.ptr = malloc(size);
        if (!args.ptr)
            return NULL;
        return args.ptr;
    }

    /* Restore sys hooks */
    __malloc_hook = sys_malloc_hook;
    __free_hook = sys_free_hook;

    args.size = size;

    nmalloc(&args);
//    pthread_create(&thread, NULL, &nmalloc, (void*) &args);
//    pthread_join(thread, NULL);
//    pthread_attr_destroy(&attr);

    /* Restore nmalloc hooks */
    __free_hook = nfree_hook;
    __malloc_hook = nmalloc_hook;

   return args.ptr;
}
void *nmalloc(void *arguments)
{
    register short index = 0;
    short padding = 0, offset;
    size_t realSize;
    unsigned long *guard_page = NULL;
    struct arg_struct *args = (struct arg_struct*) arguments;

    realSize = args->size + (PAGESIZE*2);
    args->ptr = malloc(realSize);
    if (!args->ptr)
        return NULL;

    /*
     * mprotect requires address to be aligned to a page boundary
     * the padding aligns the guard_page to that boundary
     */
    offset = (unsigned long)(args->ptr + args->size) & (PAGESIZE-1);
    if (offset)
        padding = PAGESIZE - offset;

    guard_page = args->ptr + args->size + padding;
    if (!guard_page) {
        free(args->ptr);
        return NULL;
    }
    *guard_page = (unsigned long) guard_page ^ *canary;

    if (mprotect(guard_page, sizeof(guard_page), GUARD_SETTING) == -1) {
        free(args->ptr);
        return NULL;
    }

    pthread_mutex_lock(&table_mutex);
    while (index < NMALLOC_TOTAL) {
        if (!nmalloc_info[index].ptr) {
            nmalloc_info[index].ptr = args->ptr;
            nmalloc_info[index].guard_page = guard_page;
            nmalloc_info[index].size = args->size;
            nmalloc_info[index].realSize = realSize;
            break;
        }
        index++;
        if (index == NMALLOC_TOTAL)
            index = 0;
    }
    pthread_mutex_unlock(&table_mutex);

#ifdef DEBUG
    fprintf(stderr, "Debug malloc, %p %p %lu %zd %zd %lu\n", args->ptr, guard_page, *guard_page, args->size, realSize, (*canary ^ (unsigned long)guard_page));
#endif
}

inline short get_real_ptr(void *ptr)
{
    register short i;
    size_t size, realSize;
    unsigned long *guard_page = NULL;

    for (i = 0; i < NMALLOC_TOTAL; i++) {
        if (nmalloc_info[i].ptr == ptr) {
            guard_page = nmalloc_info[i].guard_page;

            if (((unsigned long) guard_page ^ *canary) != *guard_page)
                return -EBAD;

            if (mprotect(guard_page, sizeof(guard_page), PROT_READ|PROT_WRITE) == -1)
                return -EBAD;

            return i;
        }
    }
    return -EOK;

}

void nfree_hook(void *ptr, const void *caller)
{
    short index;
//    pthread_t thread;
    struct arg_struct args;

    if (!ptr) {
        return;
    } else {
        index = get_real_ptr(ptr);
    }

    /* Restore sys hooks */
    __free_hook = sys_free_hook;

    args.ptr = ptr;
    args.index = index;

    nfree(&args);
//    pthread_create(&thread, NULL, &nfree, (void*) &args);
//    pthread_join(thread, NULL);
//    pthread_attr_destroy(&attr);

    /* Restore nmalloc hooks */
    __free_hook = nfree_hook;
}
void *nfree(void *arguments)
{
    void *real_ptr = NULL;
    struct arg_struct *args = (struct arg_struct*) arguments;

    if (args->index >= 0) {
        pthread_mutex_lock(&table_mutex);
        real_ptr = nmalloc_info[args->index].ptr;
        free(real_ptr);

        memset(&nmalloc_info[args->index], 0x0, sizeof(struct nmalloc_info));
        pthread_mutex_unlock(&table_mutex);
    } else if (args->index == -EOK) {
        // a valid pointer, but probably not something setup by nmalloc
        free(args->ptr);
    }

}

void *nrealloc_hook(void *oldmem, size_t size, const void *caller)
{
//    pthread_t thread;
    struct arg_struct args;

    if (!__realloc_hook) {
        args.ptr = realloc(oldmem, size);
        if (!args.ptr)
            return NULL;
        return args.ptr;
    }

    /* Restore sys hooks */
    __free_hook = sys_free_hook;
    __realloc_hook = sys_realloc_hook;

    args.size = size;
    args.oldmem = oldmem;

    nrealloc(&args);
//    pthread_create(&thread, NULL, &nrealloc, (void*) &args);
//    pthread_join(thread, NULL);
//    pthread_attr_destroy(&attr);

    /* Restore nmalloc hooks */
    __free_hook = nfree_hook;
    __realloc_hook = nrealloc_hook;

   return args.ptr;
}
void *nrealloc(void *arguments)
{
    register short index = 0;
    short padding = 0, offset;
    size_t oldSize, realSize;
    unsigned long *guard_page = NULL;
    struct arg_struct *args = (struct arg_struct*) arguments;

    if (!args->oldmem) {
        // nmalloc_hook
        args->ptr = malloc(args->size);
        return NULL;
    } else {
        index = get_real_ptr(args->oldmem);
    }

    if (index == -EBAD) {
        return NULL;
    } else if (index == -EOK) {
        oldSize = malloc_usable_size(args->oldmem);

        // nmalloc_hook
        args->ptr = malloc(args->size);
        if (oldSize < args->size) {
            memcpy(args->ptr, args->oldmem, oldSize);
        } else {
            memcpy(args->ptr, args->oldmem, args->size);
        }
//        pthread_mutex_lock(&alloc_mutex);
        free(args->oldmem);
//        pthread_mutex_unlock(&alloc_mutex);
        return NULL;
    }

    if (index >= 0) {
        pthread_mutex_lock(&table_mutex);

        guard_page = nmalloc_info[index].guard_page;
        realSize = nmalloc_info[index].realSize;
        realSize = args->size + (PAGESIZE*2);

//        pthread_mutex_lock(&alloc_mutex);
        args->ptr = realloc(nmalloc_info[index].ptr, realSize);
//        pthread_mutex_unlock(&alloc_mutex);
        if (!args->ptr) {
            pthread_mutex_unlock(&table_mutex);
            return NULL;
        }

        offset = (unsigned long)(args->ptr + args->size) & (PAGESIZE-1);
        if (offset)
            padding = PAGESIZE - offset;

        guard_page = args->ptr + args->size + padding;
        if (!guard_page) {
//            pthread_mutex_lock(&alloc_mutex);
            free(args->ptr);
//            pthread_mutex_unlock(&alloc_mutex);
            memset(&nmalloc_info[index], 0x0, sizeof(struct nmalloc_info));
            pthread_mutex_unlock(&table_mutex);
            return NULL;
        }
        *guard_page = (unsigned long) guard_page ^ *canary;

        if (mprotect(guard_page, sizeof(guard_page), GUARD_SETTING) == -1) {
//            pthread_mutex_lock(&alloc_mutex);
            free(args->ptr);
//            pthread_mutex_unlock(&alloc_mutex);
            memset(&nmalloc_info[index], 0x0, sizeof(struct nmalloc_info));
            pthread_mutex_unlock(&table_mutex);
            return NULL;
        }

        nmalloc_info[index].ptr = args->ptr;
        nmalloc_info[index].guard_page = guard_page;
        nmalloc_info[index].size = args->size;
        nmalloc_info[index].realSize = realSize;

        pthread_mutex_unlock(&table_mutex);
#ifdef DEBUG
        fprintf(stderr, "Debug realloc, %p %p %lu %zd %zd %lu\n", args->ptr, guard_page, *guard_page, args->size, realSize, (*canary ^ (unsigned long)guard_page));
#endif
    }
    return NULL;
}

/* vim: set ts=4 sts=4 sw=4 et: */
