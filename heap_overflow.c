/* demonstrates dynamic overflow in heap (initialized data) */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFSIZE 16
#define OVERSIZE 8 /* overflow buf2 by OVERSIZE bytes */

int main()
{
   u_long diff;
   char *buf1 = (char *)malloc(BUFSIZE), *buf2 = (char *)malloc(1);
   buf2 = realloc(buf2, BUFSIZE);

   diff = (u_long)buf2 - (u_long)buf1;
   printf("buf1 = %p, buf2 = %p, diff = 0x%lx bytes\n", buf1, buf2, diff);

   memset(buf2, 'A', BUFSIZE);
   buf2[BUFSIZE-1] = '\0';

   printf("before overflow: buf2 = %s\n", buf2);
   memset(buf1, 'B', (u_int)(diff + OVERSIZE));
   printf("after overflow: buf2 = %s\n", buf2);

//   free(buf2);
   free(buf1);
   return 0;
}
