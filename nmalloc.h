/*
 * Copyright (C) 2014-2019 Free Software Foundation, Inc.
 *
 * nmalloc 4.0
 *
 * Created Jun 5, 2014 by Su Yu
 * Modified by Brian Haslett
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef _NMALLOCH
#define _NMALLOCH

#include <stdio.h>
#include <malloc.h>       /* defines malloc hooks */
#include <string.h>       /* defines memset, memcpy */
#include <unistd.h>       /* defines getpagesize */
#include <sys/mman.h>     /* defines mprotect */
#include <pthread.h>      /* defines threads and mutexes */
#include <sys/random.h>   /* defines getrandom */

__attribute__((always_inline)) short get_real_ptr(void*);
void *nmalloc(void*);
void *nfree(void *);
void *nrealloc(void *);

/* Prototypes for our hooks.  */
void nmalloc_init_hook(void);
void *nmalloc_hook(size_t, const void *);
void nfree_hook(void *, const void *);
void *nrealloc_hook(void *, size_t, const void *);

/* Variables to save original hooks. */
void *(*sys_malloc_hook)(size_t, const void *);
void (*sys_free_hook)(void *, const void *);
void *(*sys_realloc_hook)(void *, size_t, const void *);

/* Override initializing hook from the C library. */
void (*__malloc_initialize_hook) (void) = &nmalloc_init_hook;

pthread_attr_t attr;
pthread_mutex_t table_mutex;
//pthread_mutex_t alloc_mutex;
pthread_mutexattr_t tma;
//pthread_mutexattr_t ama;

#define NMALLOC_TOTAL 1024

#define GUARD_SETTING PROT_READ

#define PAGESIZE sysconf(_SC_PAGESIZE)

#define EOK 1
#define EBAD 2

struct nmalloc_info {
    size_t size;
    size_t realSize;
    void *ptr;
    unsigned long *guard_page;
} nmalloc_info[NMALLOC_TOTAL];

struct arg_struct {
    short index;
    size_t size;
    void *ptr;
    void *oldmem;
};

#endif // _NMALLOCH
